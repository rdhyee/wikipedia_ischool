# -*- coding: UTF-8 -*-

"""
a script to calculate the number of days users edit the Wikipedia

"""

from wpp_settings import WPPDEV_DB, WPPDEV_USER, WPPDEV_PW, WPPDEV_HOST, WPPDEV_PORT
from wpp_settings import WPPRY_DB, WPPRY_USER, WPPRY_PW, WPPRY_HOST, WPPRY_PORT

import logging
LOG_FILENAME = 'number_of_edit_days.log'

import unittest

# filemode='w' to recreate file

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=LOG_FILENAME,
                    filemode='w')
logging.debug('This message should go to the log file')

from wpp_newusers import mwclient_demo
import sys

def dict_cmp(d1,d2):
    """compare two dictionaries d1 and d2"""
    # first compare keys
    k1 = d1.keys()
    k2 = d2.keys()
    print "keys in common: ", set(k1) & set(k2)
    print "keys in d1 only: ", set(k1) - set(k2)
    print "keys in d2 only: ", set(k2) - set(k1)
    # identify k,v pairs that are different
    kv1 = d1.items()
    kv2 = d2.items()
    print "k/v in common: ", set(kv1) & set(kv2)
    print "k/v in d1 only: ", set(kv1) - set(kv2)
    print "k/v in d2 only: ", set(kv2) - set(kv1)
    

def filter_none_value(d):
    return dict([(k,v) for (k,v) in d.items() if v is not None])
    
def page_revisions_test():
    import itertools
    import mwclient
    mw = mwclient.Site("en.wikipedia.org")
    page = mw.Pages['Wikipedia:Sandbox']
    revisions = page.revisions(prop='ids|timestamp|flags|comment|user|content')
    for r in itertools.islice(revisions,5):
        print r
        
def revisions_test():
    """ testing a mwclient.Site.revisions method that I added """
    # currently code not working
    import mwclient
    mw = mwclient.Site("en.wikipedia.org")
    id_list = [396240352,392544274,396332337]
    revisions = mw.revisions(revids=id_list)
    print list(revisions)

def contribs_test():
    from wpp_newusers import wpp_db2
    db = wpp_db2()
    for c in db.get_usercontribs():
        print c
        
def test_wpp_db2_get_users_by_latest_usercontribs_timestamp_checked(max_to_print=100):
    from wpp_newusers import wpp_db2
    import itertools
    db = wpp_db2(user="wpp_test", pw="wpp_test", db="wpp_test", host="127.0.0.1", port=3306)
    for u in itertools.islice(db.get_users_by_latest_usercontribs_timestamp_checked(),max_to_print):
        print u

        
def wpp_db2_debug_write():
    from wpp_newusers import wpp_db2, wpp_usercontribs_updater
    db = wpp_db2(user="wpp_test", pw="wpp_test", db="wpp_test", host="127.0.0.1", port=3306)
    updater = wpp_usercontribs_updater(user="wpp_test", pw="wpp_test", db="wpp_test", host="127.0.0.1", port=3306)
    
    users = db.get_users_by_latest_usercontribs_timestamp_checked()
    contribs = updater.contribs_for_users(users)
    contrib = contribs.next()
    print contrib
    
    #db.put_usercontrib(**contrib)
    # figure out whether it's an individual element causing syntax error
    for k in contrib.keys():
        try:
            params = {}
            params["rev_id"]= contrib["rev_id"] # always have rev_id
            params[k] = contrib[k]
            db.put_usercontrib(**params)
            print "trying ", k, " success"
        except Exception, e:
            print "trying ", k, " fail ", e
        
    db.put_usercontrib(rev_id="5", page_title=None, page_id=None, user_name=None, comment=None, parsedcomment=None,
                        minor=None, patrolled=None, timestamp=None, namespace=None, tags=None, record_created=None,
                        last_updated=None, missing=False)
    
def update_usercontribs(max_to_update=3):
    from wpp_newusers import wpp_db2, wpp_usercontribs_updater
    updater = wpp_usercontribs_updater(user=WPPRY_USER,pw=WPPRY_PW,db=WPPRY_DB,host=WPPRY_HOST, port=WPPRY_PORT)
    updater.update_usercontribs_by_lastupdate(max_to_update=max_to_update)

def debug_timestamp():
    import datetime
    from wpp_newusers import wpp_db2, wpp_usercontribs_updater
    wppdb = wpp_db2(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    conn = wppdb.conn
    
    ts = datetime.datetime(2010,11,15,3,0,0)
    wppdb.put_usercontrib(rev_id="8", timestamp=ts)


def test_editcount_for_user(user_name):
    import datetime
    import sys
    from wpp_newusers import wpp_db2, wpp_usercontribs_updater
    db2 = wpp_db2(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    c_updater = wpp_usercontribs_updater(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)

    #It seems that the presence of microseconds is problematic for mwclient.
    #mwclient is currently converting datetime to a string via str and subseconds is causing problems in the mediawiki api
    #for now truncate the microseconds 
    
    # end = datetime.datetime(*datetime.datetime.utcnow().timetuple()[:6])
    end = "2010-11-15 13:59:39"
    time_limits = filter_none_value({'start':None, 'end':end})
    dir = 'newer'
    contribs = list(c_updater.mw.usercontributions(user=user_name, prop="ids|title|timestamp|comment|parsedcomment|size|flags|tags",dir=dir, **time_limits))
    print "test_editcount_for_user->username: ", user_name, " | len(contribs): ", len(contribs)    
    
    
def update_specific_users(user_names):
    import datetime
    import sys
    from wpp_newusers import wpp_db2, wpp_usercontribs_updater, wpp_newusers_updater
    db2 = wpp_db2(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    c_updater = wpp_usercontribs_updater(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    u_updater = wpp_newusers_updater(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    
    # I'm having problems using datetime.datetime.utcnow() for end
    #end = datetime.datetime.utcnow()
    # It seems that the presence of microseconds is problematic for mwclient.
    # mwclient is currently converting datetime to a string via str and subseconds is causing problems in the mediawiki api
    # for now truncate the microseconds 
    
    #end = datetime.datetime(*datetime.datetime.utcnow().timetuple()[:6])
    #time_limits = filter_none_value({'start':None, 'end':end})
    #dir = 'newer'
    #contribs = list(c_updater.mw.usercontributions(user=user_name, prop="ids|title|timestamp|comment|parsedcomment|size|flags|tags",dir=dir, **time_limits))
    #print len(contribs)
    
    # run the u_updater first and then run the c_updater  (I was making conceptual mistakes in thinking that by the copy of user
    # in memory is synchonrized to that on disk -- I think this where something like sqlalchemy can help.)
    
    u_updater.update_users(db2.get_users_by_name(user_names))
    c_updater.update_contribs_of_users(db2.get_users_by_name(user_names))
        
    
    for user in db2.get_users_by_name(user_names):
        # list stats on before
        name = user["name"]
        contribs = list(c_updater.contribs_for_users(users=[user], start=None))
        print name, " | number of contribs: ", len(contribs), " | last_updated: ", user["last_updated"] , 
        #for (m,contrib) in enumerate(contribs):
        #    print m, contrib
        # check the editcount for the user
        user_info = list(c_updater.mw.users([name],prop="blockinfo|groups|editcount|registration|emailable|gender"))
        print " | editcount: ", user_info[0]["editcount"]
        

def test_get_users_with_editcount_fewer_contribs(max_to_process=3):
    import itertools
    from wpp_newusers import wpp_db2
    db2 = wpp_db2(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)

    users = db2.get_users_with_editcount_fewer_contribs(max_to_process)
    for user in users:
        print user

def test_update_users_with_editcount_fewer_contribs(max_to_process=3):
    import itertools
    from wpp_newusers import wpp_db2, wpp_newusers_updater, wpp_usercontribs_updater
    db2 = wpp_db2(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    c_updater = wpp_usercontribs_updater(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    u_updater = wpp_newusers_updater(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    
    print "running test_update_users_with_editcount_fewer_contribs"
 

    # not quite right but will do
    users = db2.get_users_with_editcount_fewer_contribs(max_to_process)
    (users0, users1) = itertools.tee(users,2)
        

    # update users
    print "running u_updater"
    u_updater.update_users(users0)
    
    # now calculate the names
    print "running c_updater"
    user_names = itertools.imap(lambda x: x["name"], users1)
    # copy the names off
    (user_names0, user_names1) = itertools.tee(user_names)
    
    c_updater.update_contribs_of_users(db2.get_users_by_name(user_names0))
    
    # print names
    print list(user_names1)
    
def test_itertools_use():
    import itertools
    r = xrange(100)
    (u1,u2) = itertools.tee(r,2)
    sq2 = itertools.imap(lambda x: x**2, u2)
    for k in u1:
        print k
    for l in sq2:
        print l
        
class Test_wpp_newusers(unittest.TestCase):
    
    def N_test_contribs_for_users_specific(self):
        """test contribs_for_users_2 by comparing with contribs_for_users"""
        import datetime
        import itertools
        from wpp_newusers import wpp_db2, wpp_usercontribs_updater
        
        c_updater = wpp_usercontribs_updater(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
        db2 = wpp_db2(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
        sample_size = 32
        
        def contribs_to_set(contribs):
            s = set()
            for c in contribs:
                if isinstance(c,dict):
                    s.add((c["rev_id"], str(datetime.datetime(*c["timestamp"][:6]))))
                else:
                    s.add(str(c))
            return s
                    
        nowish = datetime.datetime(*datetime.datetime.utcnow().timetuple()[:6])

        #users0 = itertools.islice(db2.get_users_by_latest_usercontribs_timestamp_checked(),sample_size)
        users0 = db2.get_users_by_name([u"Xenoknight"])
        (users1,users2) = itertools.tee(users0,2)
        
        # for this test case, I'm going to have a relatively small list and will convert users1, users2 to lists
        users1 = list(users1)
        users2 = list(users2)
        
        # now a series of tests:  default start, end
        # Are we getting the same contribs but just in different order?
        contribs1 = list(c_updater.contribs_for_users(users1,add_EndOfUserToken=True))
        contribs2 = list(c_updater.contribs_for_users_2(users2,add_EndOfUserToken=True))
        s1 = contribs_to_set(contribs1)
        s2 = contribs_to_set(contribs2)
        self.assertEqual(s1,s2)
        
        # go through all contribs for users
        contribs1 = list(c_updater.contribs_for_users(users1,start=None, end=nowish, add_EndOfUserToken=True))
        contribs2 = list(c_updater.contribs_for_users_2(users2,start=None, end=nowish, add_EndOfUserToken=True))
        s1 = contribs_to_set(contribs1)
        s2 = contribs_to_set(contribs2)
        self.assertEqual(s1,s2)
        
        # start=end
        contribs1 = list(c_updater.contribs_for_users(users1,start=nowish, end=nowish, add_EndOfUserToken=True))
        contribs2 = list(c_updater.contribs_for_users_2(users2,start=nowish, end=nowish, add_EndOfUserToken=True))
        s1 = contribs_to_set(contribs1)
        s2 = contribs_to_set(contribs2)
        self.assertEqual(s1,s2)
        
        # end=nowish
        contribs1 = list(c_updater.contribs_for_users(users1,end=nowish, add_EndOfUserToken=True))
        contribs2 = list(c_updater.contribs_for_users_2(users2, end=nowish, add_EndOfUserToken=True))
        s1 = contribs_to_set(contribs1)
        s2 = contribs_to_set(contribs2)
        self.assertEqual(s1,s2)
            
    
    def test_contribs_for_users_2(self):
        """test contribs_for_users_2 by comparing with contribs_for_users"""
        import datetime
        import itertools
        from wpp_newusers import wpp_db2, wpp_usercontribs_updater
        
        c_updater = wpp_usercontribs_updater(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
        db2 = wpp_db2(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
        sample_size = 256
        
        def contribs_to_set(contribs):
            s = set()
            for c in contribs:
                if isinstance(c,dict):
                    s.add((c["rev_id"], str(datetime.datetime(*c["timestamp"][:6]))))
                else:
                    s.add(str(c))
            return s
                    
        nowish = datetime.datetime(*datetime.datetime.utcnow().timetuple()[:6])

        users0 = itertools.islice(db2.get_users_by_latest_usercontribs_timestamp_checked(),sample_size)
        (users1,users2) = itertools.tee(users0,2)
        
        # for this test case, I'm going to have a relatively small list and will convert users1, users2 to lists
        users1 = list(users1)
        users2 = list(users2)
        
        # now a series of tests:  default start, end
        # Are we getting the same contribs but just in different order?
        contribs1 = list(c_updater.contribs_for_users(users1,add_EndOfUserToken=True))
        contribs2 = list(c_updater.contribs_for_users_2(users2,add_EndOfUserToken=True))
        s1 = contribs_to_set(contribs1)
        s2 = contribs_to_set(contribs2)
        logging.debug("default start, end ")
        logging.debug("s1: %s" % (s1))
        logging.debug("s2: %s" % (s2))
        self.assertEqual(s1,s2)
        
        # go through all contribs for users
        contribs1 = list(c_updater.contribs_for_users(users1,start=None, end=nowish, add_EndOfUserToken=True))
        contribs2 = list(c_updater.contribs_for_users_2(users2,start=None, end=nowish, add_EndOfUserToken=True))
        s1 = contribs_to_set(contribs1)
        s2 = contribs_to_set(contribs2)
        self.assertEqual(s1,s2)
        
        # start=end
        contribs1 = list(c_updater.contribs_for_users(users1,start=nowish, end=nowish, add_EndOfUserToken=True))
        contribs2 = list(c_updater.contribs_for_users_2(users2,start=nowish, end=nowish, add_EndOfUserToken=True))
        s1 = contribs_to_set(contribs1)
        s2 = contribs_to_set(contribs2)
        self.assertEqual(s1,s2)
        
        # end=nowish
        contribs1 = list(c_updater.contribs_for_users(users1,end=nowish, add_EndOfUserToken=True))
        contribs2 = list(c_updater.contribs_for_users_2(users2, end=nowish, add_EndOfUserToken=True))
        s1 = contribs_to_set(contribs1)
        s2 = contribs_to_set(contribs2)
        self.assertEqual(s1,s2)
        
    
        #print "calculating contribs1" , str(datetime.datetime.now())
        #print "calculating contribs2" , str(datetime.datetime.now())
        #print "finished contrib2 ", str(datetime.datetime.now())
        #    
        #print "contribs1 == contribs2", contribs1 == contribs2
        #print len(contribs1), len(contribs2)
        
        #print "contribs1 == contribs2", contribs1 == contribs2
        #if (contribs1 != contribs2):
        #    for (m, (c1,c2)) in enumerate(itertools.izip(contribs1,contribs2)):
        #        if c1 != c2:
        #            print "first diff between contrib1, contrib2", m, c1, c2
        #            break
        #print "s1 == s2", s1 == s2
        #print "s1, s2 common", s1 & s2
        #print "unique to s1", s1 - s2
        #print "unique to s2", s2 - s1
        #
        #print "contribs1"
        #for (m,k1) in enumerate(contribs1):
        #    if (isinstance(k1,dict)):
        #        print m, k1["user_name"], k1["rev_id"], str(datetime.datetime(*k1["timestamp"][:6]))
        #    else:
        #        print str(k1)
        #
        #print 
        #print "contribs2"
        #for (m,k1) in enumerate(contribs2):
        #    if (isinstance(k1,dict)):
        #        print m, k1["user_name"], k1["rev_id"], str(datetime.datetime(*k1["timestamp"][:6]))
        #    else:
        #        print str(k1)


def contribs_for_users(user_names):
    import datetime
    import itertools
    from wpp_newusers import wpp_db2, wpp_usercontribs_updater
    
    c_updater = wpp_usercontribs_updater(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    db2 = wpp_db2(user="wpp_test",pw="wpp_test",db="wpp_test",host="127.0.0.1", port=3306)
    users = db2.get_users_by_name(user_names)
    contribs = list(c_updater.contribs_for_users_2(users,add_EndOfUserToken=True))
    for c in contribs:
        print c
    

    
def test():
    #test_itertools_use()
    #update_specific_users(user_names=["Babia Góra"])
    #test_update_users_with_editcount_fewer_contribs(1000)
    #test_get_users_with_editcount_fewer_contribs(10)
    #test_contribs_for_users_2()
    unittest.main()
    #contribs_for_users(user_names=[u"Babia Góra"])
    
    
if __name__ == '__main__':
    #page_revisions_test()
    #revisions_test()
    #contribs_test()
    #wpp_db2_write_test()
    #test_wpp_db2_get_users_by_latest_usercontribs_timestamp_checked(10)
    #debug_timestamp()
    
    #test()
    #sys.exit()
    
    #update_specific_users(user_names=["1000ThangLong", "0wendy33"])
    #test_editcount_for_user(user_name="0wendy33")
    # the updater to run
    update_usercontribs(1000000)


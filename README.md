To Do
=====

* Write a command to invite a specified number of accounts within a given stratum,
that have not yet been invited. A possible approach is to modify the invitation
function (`SurveyInviter.invite`?) to accept a "blacklist" -- list of accounts
to not sample from.

* command to send reminders. -- I have a command to send generic emails out
  (`wpp_dashboard.send_email_to_wikipedia_accounts`), but let's now write a command to identify
  which email accounts to send to in various scenarios.
      
    * Who needs reminders? every stratum with the following states: `INVITED`
      and/or `SURVEY1_STARTED` -- we can calculate when the invitations expire and send out
      reminders some given number of days before the expiration date.
     
* command to cancel out outstanding invitations, say when we don't need any more
  for a stratum
  
   * Which invitations need to be expired? How to expire them? well, one could delete
     the tokens or artificially move the deadline to now -- but we might want to
     have a way of marking a token expired so that should someone come around to
     try to use it, we can acknowledge sending it out but we no longer need it.
     (Is there a faciity in limesurvey to do so?
     http://docs.limesurvey.org/Closing+a+survey#Expiry might work -- but we're
     just shutting down a particular stratum, not necessarily all strata -- so
     expiring a survey as a whole is not the solution) The best solution so far:
     reset the expiration date on a token to now...

* calculate the stats of the overall sample pool to confirm the distribution of
  the strata.  Remember that the current survey methodology is based on some preliminary
  calculation of the statistics of Wikipedia participants.  It'd be good to recalculate them,
  though strictly speaking, our stratified sampling should work regardless of the underlying stats, right -- and
  hence, you can calculate the distributions after the fact.  However, it'd be better to calculate
  stats ahead of time to optimize our sampling.
  
* handle survey2

* later on: something to help with sending out amazon gift certificates

Done:

* *Done*: For each stratum, break down the number of accounts in each state of working
  through the survey (e.g., not started, started, finished, expired), so it's
  relatively easy to spot how many more invitations need to be issued.  Current
  state:
  
        from wpp_dashboard import Dashboard
        d = Dashboard()
        d.state_by_strata()
    
  yields:
    
        Out[7]: 
        {-1: {'EXPIRED_INVITATION': [u'RaymondYee'],
          'INVITED': [],
          'NON_PARTICIPANT': [],
          'SURVEY1_DONE': [],
          'SURVEY1_EXPIRED': [],
          'SURVEY1_STARTED': []},
         0: {'EXPIRED_INVITATION': [],
          'INVITED': [],
          'NON_PARTICIPANT': [],
          'SURVEY1_DONE': [],
          'SURVEY1_EXPIRED': [],
          'SURVEY1_STARTED': []},
         1: {'EXPIRED_INVITATION': [],
          'INVITED': [],
          'NON_PARTICIPANT': [],
          'SURVEY1_DONE': [],
          'SURVEY1_EXPIRED': [],
          'SURVEY1_STARTED': []},
         2: {'EXPIRED_INVITATION': [],
          'INVITED': [],
          'NON_PARTICIPANT': [],
          'SURVEY1_DONE': [],
          'SURVEY1_EXPIRED': [],
          'SURVEY1_STARTED': []},
         3: {'EXPIRED_INVITATION': [],
          'INVITED': [],
          'NON_PARTICIPANT': [],
          'SURVEY1_DONE': [],
          'SURVEY1_EXPIRED': [],
          'SURVEY1_STARTED': []},
         4: {'EXPIRED_INVITATION': [],
          'INVITED': [],
          'NON_PARTICIPANT': [],
          'SURVEY1_DONE': [],
          'SURVEY1_EXPIRED': [],
          'SURVEY1_STARTED': []}}


  When we send invitations out, we should mark in the db what stratum and underlying
  number of contribs done (so that I can restratify if need be) -- have it in
  wpp_participants.count_contrib; maybe add a stratum number also to make
  display easier. 


WPP Documentation
=================

repository on bitbucket: https://bitbucket.org/rdhyee/wikipedia_ischool

The `wikipedia_ischool` source code is available also in a working directory on
`granite`:

    /home/raymond/wikipedia_ischool
    
   
[Best documentation on our survey design](https://docs.google.com/document/d/1FNCrsCEL0qx9Z9s3IdURL6dNs_KA6FUQm9fLlTqkUCI/edit)

    
# Setup notes

I'm using [virtualenv](http://www.virtualenv.org/en/latest/index.html) and
[virtualenvwrapper](http://www.doughellmann.com/docs/virtualenvwrapper/). The
various environments are at: `granite:/home/raymond/.virtualenvs/`  -- specifically,
I have the requisite libraries listed in
[requirements.pip](https://bitbucket.org/rdhyee/wikipedia_ischool/src/master/requirements.pip)
installed in the `wpp` environment

To install the libraries:

    pip install -r requirements.pip
    
To use the `wpp` environment:

    workon wpp
    
# mwclient

I'm using a modified version of
[mwclient](http://sourceforge.net/projects/mwclient/) to access the Wikipedia
API, primarily to handle emailing to wikipedia accounts: [my fork of
mwclient](https://bitbucket.org/rdhyee/mwclient)

# wpp_bottle

`wpp_bottle.py` is the web application that I've written so far to display the status of surveys and pull up
 a sample of accounts to possibly invite to the survey. The web framework used is the Python web microframework
[Bottle](http://bottlepy.org/).

## how to launch the web app
Here's the incantation I use to start the bottle web application:

    screen
    workon wpp
    python wpp_bottle.py

Two web pages:

## dashboard

* [http://granite.ischool.berkeley.edu:8080/dashboard](http://granite.ischool.berkeley.edu:8080/dashboard)

[code for dashboard](https://bitbucket.org/rdhyee/wikipedia_ischool/src/019c6d57b9cc/wpp_bottle.py#cl-53)

shows the status of invitiations in the database, divided into strata.

## sample

 * [http://granite.ischool.berkeley.edu:8080/sample](http://granite.ischool.berkeley.edu:8080/sample)

[code for sample](https://bitbucket.org/rdhyee/wikipedia_ischool/src/019c6d57b9cc/wpp_bottle.py#cl-26)

a sample calculated for each stratum, including a "special" stratum -- reload to get a new sample
    
# crawl

At the beginning of each week, run the [crawl.sh](https://bitbucket.org/rdhyee/wikipedia_ischool/src/master/crawl.sh)
in `granite:/home/raymond/wikipedia_ischool/crawl.sh`
    
For production purposes, we should look at various [deployment
configurations](http://bottlepy.org/docs/dev/tutorial.html#deployment). Get an
app to run through Apache + wsgi?



    
    
%import random, urllib
<html>
  <head>
    <title>Sample</title>
    <link rel="stylesheet" href="/static/wpp.css" type="text/css">
  </head>
  <body>
     <p>Sample</p>
%# Right now, set an arbitrary number of accounts to sample / stratum
%sample_n = 30 
     <ul>
%for (k,stratum) in enumerate(strata):
        <li>
             <div>stratum {{k}}: {{divs[k]}}</div>
             <ol id="id">
%for (i,user) in enumerate(random.sample(stratum,sample_n)):
              <li><a href="http://en.wikipedia.org/wiki/Special:Contributions/{{urllib.quote(user["name"])}}">{{user["name"]}}</a> {{user["count_contrib"]}} {{user["registration"]}}</li>
%end
             </ol>
        </li>
%end
     </ul>
     <div>Special samples</div>
     <form method="post" action="#">
        <ul>
   %for (k,sample) in enumerate(special_sample_ids):      
         <li>
          <label><input type="checkbox" name="to_invite" value="{{sample.wikipedia_id}}"/>
          <a href="http://en.wikipedia.org/wiki/Special:Contributions/{{urllib.quote(sample.wikipedia_id)}}">{{sample.wikipedia_id}}</a>
          </label>
         </li>
   %end
        </ul>
        <input type="submit" value="Send invitation!"/>
     </form>
  </body>
</html>
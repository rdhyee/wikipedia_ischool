%import random, urllib
<html>
  <head>
    <title>Sample</title>
    <link rel="stylesheet" href="/static/wpp.css" type="text/css">
  </head>
  <body>
     <p>Sample Results</p>
     <ul>
%for (k,result) in enumerate(results):
        <li>
             <div>{{result["user_name"]}} {{result["success"]}} {{result["error_message"]}}</div>
        </li>
%end
     </ul>
  </body>
</html>

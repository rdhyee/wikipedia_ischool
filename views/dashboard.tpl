<html>
  <head>
    <title>Dashboard</title>
    <link rel="stylesheet" href="/static/wpp.css" type="text/css">
  </head>
  <body>
    <p>Number of invitations issued in total: {{dashboard.number_of_invitations()}}</p>
    
    <div class="header">All invitees</div>
%for p in dashboard.invitees():
    <p>{{p.wikipedia_id}} {{p.survey1_token}} {{p.recruit_msg_timestamp}}</p>
%end

%# Invited

    <div class="header">Invited</div>
%results = list(dashboard.invited())
%if results:
        <ul>
    %for result in results:
            <li>{{result.wikipedia_id}} {{result.survey1_token}} {{result.validfrom}} {{result.validuntil}}</li>
    %end
        </ul>
%else:
      <p>No Invited</p>
%end

    <div class="header">Survey 1 Started</div>
%results = list(dashboard.survey1_started())
%if results:
        <ul>
    %for result in results:
            <li>{{result.wikipedia_id}} {{result.survey1_token}} {{result.validfrom}} {{result.validuntil}}</li>
    %end
        </ul>
%else:
      <p>No Survey 1 started</p>
%end


    <div class="header">Expired Invitations</div>
%results = list(dashboard.expired_invitation())
%if results:
        <ul>
    %for result in results:
            <li>{{result.wikipedia_id}} {{result.survey1_token}} {{result.validfrom}} {{result.validuntil}}</li>
    %end
        </ul>
%else:
      <p>No Expired Invitations</p>
%end

    <div class="header">Survey 1 Done</div>
%results = list(dashboard.survey1_done())
%if results:
        <ul>
    %for result in results:
            <li>{{result.wikipedia_id}} {{result.survey1_token}} {{result.validfrom}} {{result.validuntil}}</li>
    %end
        </ul>
%else:
      <p>No Survey1 Done</p>
%end

%# Survey1 Expired

    <div class="header">Survey 1 Expired</div>
%results = list(dashboard.survey1_expired())
%if results:
        <ul>
    %for result in results:
            <li>{{result.wikipedia_id}} {{result.survey1_token}} {{result.validfrom}} {{result.validuntil}}</li>
    %end
        </ul>
%else:
      <p>No Survey1 Expired</p>
%end



  </body>
</html>
SURVEY_ID = 88653

INVITATION_SUBJECT_LINE = """Survey Research: UC Berkeley Study of New Editors"""

INVITATION_BODY = """The University of California, Berkeley is conducting social research to better understand how people use Internet-based systems such as Wikipedia. We are contacting you to see if you are interested in helping us with social research about Wikipedia editing behavior. Not only is this a great way to share your own experiences and opinions, but you will also receive up to two Amazon.com Gift Cards for your participation.
 
There are two short surveys in this study. You can complete one survey now and another that we will send by email in about a month. Each of the two surveys will take 10-15 minutes and will ask you about your experiences and attitudes on issues relating to editing and using Wikipedia. You will receive the first $10 Amazon.com Gift Card when you complete our initial survey. You will receive a second $10 Amazon.com Gift Card when you complete the follow-up survey in 4-6 weeks.
 
Your participation is entirely voluntary, and should you choose to participate, you may discontinue participation at any time at your sole discretion with no penalty or consequence to you.
 
If you are interested in participating, we invite you to read and agree to our statement of
informed consent regarding this research:

%(survey_link)s

Thank you for your interest in social research!"""

SURVEY_URL = "http://granite.ischool.berkeley.edu/wpp/web_system/survey/index.php?sid=%s&lang=en&token=%s"

WPPDEV_USER = ""
WPPDEV_PW = ""
WPPDEV_HOST = ""
WPPDEV_PORT = 3306
WPPDEV_DB = ""
WPPDEV_REMOTE_USER = ""

WPPRY_USER = ""
WPPRY_PW = ""
WPPRY_DB = ""
WPPRY_HOST = ""
WPPRY_PORT = 3306

#WIKIPEDIA_USER = "NextToGodliness"
#WIKIPEDIA_PW = "cleanisgood"

WIKIPEDIA_USER = ""
WIKIPEDIA_PW = ""

# parameterize the strata
# 0-1, 2-4, 5-8, 9-14, 15+
SAMPLE_STRATA = [(0,1), (2,4), (5,8), (9,14), (15,100000000000000000000)]


try:
    from wpp_settings_local import *
except:
    pass
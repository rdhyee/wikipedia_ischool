"""
wpp_dashboard.py

It may or may not be worthwhile to have a Python program instantiate the ssh tunnel here in conjunction with sqlalchemy.

For now I create a tunnel by hand:

    ssh -fNg -L 3307:127.0.0.1:3306 raymond@granite.ischool.berkeley.edu
    
The dashboard should have information on the following:

* who has been invited  (each record in `wpp_participants` represents an invitation)
* what invitations are still outstanding 
* who has accepted the invitation
* who has finished survey?
* which stratum a given user is from 
* how many people we want from each strata and where we are in making progress towards that sample
* ability to see summaries of responses and well as ability to drill down to individual responses.

"""

from wpp_settings import SURVEY_URL, SURVEY_ID, INVITATION_SUBJECT_LINE, INVITATION_BODY, WPPDEV_USER, WPPDEV_PW, WPPDEV_DB, \
      WPPDEV_HOST, WPPDEV_REMOTE_USER, WPPDEV_PORT
from wpp_settings import WPPRY_USER, WPPRY_PW, WPPRY_DB, WPPRY_HOST, WPPRY_PORT
from wpp_settings import WIKIPEDIA_USER, WIKIPEDIA_PW
from wpp_settings import SAMPLE_STRATA

import datetime
import itertools

from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String, Text, Sequence, and_
from sqlalchemy.orm import mapper, sessionmaker, aliased, outerjoin
from pprint import pprint

import pytz
import random
import string
import sys
import mwclient
from collections import defaultdict
from itertools import izip, count
from functools import partial

import unittest

import logging
logging.basicConfig(filename='wpp_dashboard.log', level=logging.WARN)
logger = logging.getLogger(__name__)


PARTICIPANT_STATES = ['INVITED', 'NON_PARTICIPANT', 'SURVEY1_STARTED', 'EXPIRED_INVITATION', 'SURVEY1_DONE', 'SURVEY1_EXPIRED']

# http://stackoverflow.com/questions/2348317/how-to-write-a-pager-for-python-iterators/2350904#2350904        
def grouper(iterable, page_size):
    page= []
    for item in iterable:
        page.append( item )
        if len(page) == page_size:
            yield page
            page= []
    if len(page) > 0:
        yield page

#http://stackoverflow.com/questions/42558/python-and-the-singleton-pattern/2752280#2752280
def singleton(cls):
    instances = {}
    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance

def nowish_tz():
    # put in Pacific time
    tz_PT = pytz.timezone("US/Pacific")
    return datetime.datetime(*datetime.datetime.utcnow().timetuple()[:6]).replace(tzinfo=pytz.utc).astimezone(tz_PT)
        

class WppParticipant(object):
    """CREATE TABLE `wpp_participants` (
        `wpp_id` int(11) NOT NULL AUTO_INCREMENT,
        `wikipedia_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
        `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `ip` int(10) unsigned DEFAULT NULL,
        `hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
        `hash_valid` tinyint(1) DEFAULT '0',
        `survey_completed` tinyint(1) DEFAULT '0',
        `recruit_msg_timestamp` datetime DEFAULT NULL,
        `consent_timestamp` datetime DEFAULT NULL,
        `initial_survey_complete_timestamp` datetime DEFAULT NULL,
        `second_survey_recruit_timestamp` datetime DEFAULT NULL,
        `second_survey_complete_timestamp` datetime DEFAULT NULL,
        `survey1_token` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
        `survey2_token` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
        `count_contrib` mediumint(8) unsigned DEFAULT NULL,
        `stratum` int(11) NOT NULL DEFAULT '-1',
        PRIMARY KEY (`wpp_id`),
        UNIQUE KEY `wikipedia_id` (`wikipedia_id`)
      ) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    """
    pass


class LimeSurvey(object):
    """
    CREATE TABLE `lime_survey_88653` (
      `id` int(11) NOT NULL AUTO_INCREMENT,
      `submitdate` datetime DEFAULT NULL,
      `lastpage` int(11) DEFAULT NULL,
      `startlanguage` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
      `token` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
      `datestamp` datetime NOT NULL,
      `startdate` datetime NOT NULL,
      `ipaddr` text COLLATE utf8_unicode_ci,
      `refurl` text COLLATE utf8_unicode_ci,
      `88653X4X8` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X4X9` double DEFAULT NULL,
      `88653X4X10` text COLLATE utf8_unicode_ci,
      `88653X9X15` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X9X16` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X9X17` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X9X18` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X9X19` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X121#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X121#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X122#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X122#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X123#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X123#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X124#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X124#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X125#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X125#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X126#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X5X126#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X8X149#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X8X149#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X8X1410#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X8X1410#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X8X1413#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X8X1413#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X8X1414#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X8X1414#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X111#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X111#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X112#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X112#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X113#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X113#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X114#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X114#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X115#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X115#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X116#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X116#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X117#0` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X6X117#1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X7X131` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X7X132` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X7X133` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X7X134` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6t1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6c1` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6t2` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6t3` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6c2` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6t4` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6c3` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6t5` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6c4` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X3X6c5` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X201` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X202` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X203` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X204` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X205` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X206` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X207` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X208` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X209` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X2010` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X10X2011` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X2X2` double DEFAULT NULL,
      `88653X2X3` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X2X5` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X2X5other` text COLLATE utf8_unicode_ci,
      `88653X2X7` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
      `88653X2X4` double DEFAULT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;    
    """
    pass
class LimeTokens(object):
    """
    I don't think we mess with how this is defined by LimeSurvey itself:
    CREATE TABLE `lime_tokens_88653` (
      `tid` int(11) NOT NULL AUTO_INCREMENT,
      `firstname` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
      `lastname` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
      `email` text COLLATE utf8_unicode_ci,
      `emailstatus` text COLLATE utf8_unicode_ci,
      `token` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
      `language` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
      `sent` varchar(17) COLLATE utf8_unicode_ci DEFAULT 'N',
      `remindersent` varchar(17) COLLATE utf8_unicode_ci DEFAULT 'N',
      `remindercount` int(11) DEFAULT '0',
      `completed` varchar(17) COLLATE utf8_unicode_ci DEFAULT 'N',
      `usesleft` int(11) DEFAULT '1',
      `validfrom` datetime DEFAULT NULL,
      `validuntil` datetime DEFAULT NULL,
      `mpid` int(11) DEFAULT NULL,
      PRIMARY KEY (`tid`),
      KEY `lime_tokens_88653_idx` (`token`),
      KEY `idx_lime_tokens_88653_efl` (`email`(120),`firstname`,`lastname`)
    ) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
    """    
    pass

class WppUserContrib(object):
    """
        CREATE TABLE `wpp_usercontribs` (
      `rev_id` int(10) NOT NULL COMMENT 'revision id (revid)',
      `page_title` text COMMENT 'title of page revised',
      `page_id` int(10) DEFAULT NULL,
      `user_name` varchar(255) DEFAULT NULL COMMENT 'user account responsible for rev',
      `comment` text,
      `parsedcomment` text,
      `minor` tinyint(3) unsigned DEFAULT NULL,
      `commenthidden` tinyint(4) NOT NULL,
      `patrolled` tinyint(3) unsigned DEFAULT NULL,
      `timestamp` datetime DEFAULT NULL,
      `namespace` text,
      `tags` text,
      `record_created` datetime DEFAULT NULL,
      `last_updated` datetime DEFAULT NULL,
      `missing` tinyint(4) NOT NULL,
      PRIMARY KEY (`rev_id`),
      KEY `user_name` (`user_name`)
    ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
    """
    pass
        
class WppSpecialSampling(object):
    """
        CREATE TABLE `wpp_special_sampling` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `wikipedia_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;    
    """
    pass
        
# will I be able to map more than one database simultaneously?

@singleton
class wppdev(object):
    def __init__(self, user=WPPDEV_USER, pw=WPPDEV_PW, db=WPPDEV_DB, host=WPPDEV_HOST, remote_user=WPPDEV_REMOTE_USER, port=WPPDEV_PORT,
                 survey_number=SURVEY_ID, echo=False):
    
        #mysql_connect_path = "mysql+mysqldb://%s:%s@%s:%s/%s?charset=utf8" % (user,pw,host,port,db)
        mysql_connect_path = "mysql+pymysql://%s:%s@%s:%s/%s?charset=utf8" % (user,pw,host,port,db)
        engine = create_engine(mysql_connect_path, echo=echo)
     
        metadata = MetaData(engine)
        
        # wpp_participants
        wpp_participant_table = Table('wpp_participants', metadata, autoload=True)
        mapper(WppParticipant, wpp_participant_table)
        
        # wpp_special_sampling
        wpp_special_sampling_table = Table('wpp_special_sampling', metadata, autoload=True)
        mapper(WppSpecialSampling, wpp_special_sampling_table)
        
        # lime_survey_{SURVEY_NUMBER}
        LIME_SURVEY_TABLE_NAME = "lime_survey_%s" % (str(survey_number))
        lime_survey_table = Table(LIME_SURVEY_TABLE_NAME, metadata, autoload=True)
        mapper(LimeSurvey, lime_survey_table)
        
        # lime_tokens_{SURVEY_NUMBER}
        LIME_TOKENS_TABLE_NAME = "lime_tokens_%s" % (str(survey_number))
        lime_tokens_table = Table(LIME_TOKENS_TABLE_NAME, metadata, autoload=True)
        mapper(LimeTokens, lime_tokens_table)
        
        Session = sessionmaker(bind=engine)
        session = Session()
        self.session = session
        self.survey_number = survey_number

@singleton       
class wppry(object):
    def __init__(self, user=WPPRY_USER, pw=WPPRY_PW, db=WPPRY_DB, host=WPPRY_HOST, port=WPPRY_PORT):
    
        mysql_connect_path = "mysql+mysqldb://%s:%s@%s:%s/%s?charset=utf8" % (user,pw,host,port,db)
        engine = create_engine(mysql_connect_path, echo=False)
     
        metadata = MetaData(engine)
        
        # wpp_usercontribs
        wpp_usercontribs_table = Table('wpp_usercontribs', metadata, autoload=True)
        mapper(WppUserContrib, wpp_usercontribs_table)
        
        Session = sessionmaker(bind=engine)
        session = Session()
        self.session = session
    
def basic_test():
    wppry_db = wppry()
    wppdev_db = wppdev()

    # remote db
    participants = wppdev_db.session.query(WppParticipant).all()
    for p in participants:
        print p.wikipedia_id, p.survey1_token
    
    usercontribs = wppry_db.session.query(WppUserContrib.rev_id, WppUserContrib.page_title)[0:10]
    
    for u in usercontribs:
        print u
        

        
class Dashboard(object):
        # we have to see which participants are not represented in the lime_survey_? table
        # what user states are there? Let me consult my finite state machine:  INVITED, EXPIRED_INVITATION, SURVEY1_STARTED, SURVEY1_EXPIRED, SURVEY1_DONE
        # Possible transitions
        # INVITED -> EXPIRED_INVITATION
        # INVITED -> SURVEY1_STARTED
        # SURVEY1_STARTED -> SURVEY1_DONE
        # SURVEY1_STARTED -> SURVEY1_EXPIRED
    def __init__(self):
        self.wppdev_db = wppdev()
    def number_of_invitations(self):
        # number of invitations = number of records in wpp_participants
        return self.wppdev_db.session.query(WppParticipant).count()
    def invitees(self):
        # TO DO: handle more gracefully large number of invitees
        participants = self.wppdev_db.session.query(WppParticipant).all()
        return participants
        # I don't know which of the other fields gets filled in by LimeSurvey
        # email -- not captured until the survey (which question?)

        # ip -- I don't think we get that unless we set up some sort of intermediate landing/redirect page that then goes to limesurvey -- unless limesurvey
        #       captures this info somewhere. -- yes it does in lime_survey_?
        
        # hash, hash_valid -- I think these fields have been superceded by survey1_token
        # survey_completed, initial_survey_complete_timestamp -- have to figure out how to know whether survey is completed
        # survey2_token, second_survey_complete_timestamp
        # which fields might be out of date, redundant and therefore should be amended or removed
        # also I need to add fields about the survey stratum and perhaps the quantity (# of editcounts?) that decided which stratum was calculated

    def state_of_invitation(self,wikipedia_id):
        # first check whether wikipedia_id among WppParticipant (there is either 0 or 1)
        # INVITED: in wpp_participants, no corresponding token in lime_survey_?, and lime_tokens.validuntil has not yet passed
        # EXPIRED_INVITATION: in wpp_participants, no corresponding token in lime_survey_?, and lime_tokens.validuntil has passed
        # SURVEY1_STARTED: in wpp_participants, corresponding token in lime_survey_? but lime_survey_?.submitdate is null, and lime_tokens.validuntil has not yet passed
        # SURVEY1_DONE: in wpp_participants, corresponding token in lime_survey_? lime_survey_?.submitdate is not null.
        # SURVEY1_EXPIRED: in wpp_participants, corresponding token in lime_survey_? but lime_survey_?.submitdate is null, and lime_tokens.validuntil has passed

        # this is a bit of an inefficient way perhaps -- it might be possible to do one big join based on the wikipedia id.
        
        logger.debug("Dashboard.state_of_invitation for: %s", wikipedia_id)
        nowish = nowish_tz()
        # strip the timezone off of nowish so that there's no problem in comparing the timestamps with those from the db, which are not timezone aware
        nowish = nowish.replace(tzinfo=None)
        w = aliased(WppParticipant)
        ls = aliased(LimeSurvey)
        lt = aliased(LimeTokens)
  
        # is the user in the participant database? (i.e., has the user account been invited?)
        participant = self.wppdev_db.session.query(w).filter(w.wikipedia_id == wikipedia_id).all()
        if len(participant):
            token = participant[0].survey1_token
            logger.debug("token: %s ", token)
            ltoken = self.wppdev_db.session.query(lt).filter(lt.token == token).all()
            lsurvey = self.wppdev_db.session.query(ls).filter(ls.token == token).all()

            # is there a corresponding token in lime_survey_? (ie., has user actually started the survey?)
            # what type of numeric constraints on tokens in ls?  None -- so grab all but expect 0 or 1 and throw error if more than one.
            
            if (len(lsurvey) == 0):
                # INVITED vs EXPIRED_INVITATION
                logger.debug("ltoken[0].validuntil: %s, nowish: %s", ltoken[0].validuntil, nowish)
                if ltoken[0].validuntil >= nowish:
                    return "INVITED"
                else:
                    return "EXPIRED_INVITATION"
            elif (len(lsurvey) == 1):
                # SURVEY1_STARTED vs SURVEY1_DONE vs SURVEY1_EXPIRED
                if lsurvey[0].submitdate is not None:
                    return "SURVEY1_DONE"
                else:
                    if ltoken[0].validuntil >= nowish:
                        return "SURVEY1_STARTED"
                    else:
                        return "SURVEY1_EXPIRED"
                pass
            else:
                raise Exception("More than one survey corresponding to token %s" % (survey1_token))
        else:
            return "NON_PARTICIPANT"
    def invited(self):
        # INVITED: in wpp_participants, no corresponding token in lime_survey_?, and lime_tokens.validuntil has not yet passed
        
        SQL = """
        SELECT u2.wikipedia_id, u2.survey1_token, u2.token, u2.startdate, u2.submitdate, lt.tid, lt.validfrom, lt.validuntil FROM ((SELECT * FROM 
            (SELECT w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate FROM wpp_participants w LEFT JOIN lime_survey_88653 ls ON w.survey1_token = ls.token	
            UNION
            SELECT w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate FROM wpp_participants w RIGHT JOIN lime_survey_88653 ls ON w.survey1_token = ls.token) as u
            WHERE u.token is NULL) as u2 
            LEFT JOIN lime_tokens_88653 lt ON u2.survey1_token = lt.token)
            WHERE lt.validuntil > UTC_TIMESTAMP();
        """
        nowish = nowish_tz()
        w = aliased(WppParticipant)
        ls = aliased(LimeSurvey)
        lt = aliased(LimeTokens)
        q1 = self.wppdev_db.session.query(w.wikipedia_id.label('wikipedia_id'), w.survey1_token.label('survey1_token'),
                                          ls.token.label('token'), ls.startdate.label('startdate'), ls.submitdate.label('submitdate')).select_from(outerjoin(ls, w, w.survey1_token == ls.token))
        q2 = self.wppdev_db.session.query(w.wikipedia_id.label('wikipedia_id'), w.survey1_token.label('survey1_token'),
                                          ls.token.label('token'), ls.startdate.label('startdate'), ls.submitdate.label('submitdate')).select_from(outerjoin(w, ls, w.survey1_token == ls.token))
        u = q1.union(q2).subquery()
        
        #return u
        # how to filter?
        u2 = self.wppdev_db.session.query(u).filter(u.c.token == None).subquery()
        results = self.wppdev_db.session.query(u2.c.wikipedia_id.label('wikipedia_id'), u2.c.wikipedia_id.label('survey1_token'),
                                               u2.c.token.label('token'), u2.c.startdate.label('startdate'),
                                               u2.c.submitdate.label('submitdate'), 
                                               lt.tid, lt.validfrom, lt.validuntil).select_from(outerjoin(u2, lt, u2.c.survey1_token ==lt.token)). \
                    filter(lt.validuntil >= nowish)

        return results
    def expired_invitation(self):
        # in wpp_participants, no corresponding token in lime_survey_?, and lime_tokens.validuntil has passed
        nowish = nowish_tz()
        w = aliased(WppParticipant)
        ls = aliased(LimeSurvey)
        lt = aliased(LimeTokens)
        q1 = self.wppdev_db.session.query(w.wikipedia_id.label('wikipedia_id'), w.survey1_token.label('survey1_token'),
                                          ls.token.label('token'), ls.startdate.label('startdate'), ls.submitdate.label('submitdate')).select_from(outerjoin(ls, w, w.survey1_token == ls.token))
        q2 = self.wppdev_db.session.query(w.wikipedia_id.label('wikipedia_id'), w.survey1_token.label('survey1_token'),
                                          ls.token.label('token'), ls.startdate.label('startdate'), ls.submitdate.label('submitdate')).select_from(outerjoin(w, ls, w.survey1_token == ls.token))
        u = q1.union(q2).subquery()
        
        #return u
        # how to filter?
        u2 = self.wppdev_db.session.query(u).filter(u.c.token == None).subquery()
        results = self.wppdev_db.session.query(u2.c.wikipedia_id.label('wikipedia_id'), u2.c.wikipedia_id.label('survey1_token'),
                                               u2.c.token.label('token'), u2.c.startdate.label('startdate'),
                                               u2.c.submitdate.label('submitdate'), 
                                               lt.tid, lt.validfrom, lt.validuntil).select_from(outerjoin(u2, lt, u2.c.survey1_token ==lt.token)). \
                    filter(lt.validuntil < nowish)
        return results

    def survey1_started_0(self):
        # in wpp_participants, corresponding token in lime_survey_? but lime_survey_?.submitdate is null, and lime_tokens.validuntil has not yet passed
        SQL = """
            SELECT u2.wikipedia_id, u2.survey1_token, u2.token, u2.startdate, u2.submitdate, lt.tid, lt.validfrom, lt.validuntil FROM 
            (SELECT w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate FROM wpp_participants w LEFT JOIN lime_survey_88653 ls ON w.survey1_token = ls.token WHERE ls.startdate is NOT NULL AND ls.submitdate is NULL) as u2 
            LEFT JOIN lime_tokens_88653 lt ON u2.survey1_token = lt.token
            WHERE lt.validuntil > UTC_TIMESTAMP();
        """ 
        results = self.wppdev_db.session.query('wikipedia_id', 'survey1_token', 'token', 'startdate', 'submitdate', 'tid', 'validfrom', 'validuntil'). \
                    from_statement(SQL)
        return results
    def survey1_started(self):
        # in wpp_participants, corresponding token in lime_survey_? but lime_survey_?.submitdate is null, and lime_tokens.validuntil has not yet passed
        w = aliased(WppParticipant)
        ls = aliased(LimeSurvey)
        lt = aliased(LimeTokens)
        #results = self.wppdev_db.session.query(w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate).join((ls, w.survey1_token == ls.token)). \
        #            filter(ls.submitdate == None).all()
        u2 = self.wppdev_db.session.query(w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate).select_from(outerjoin(w,ls, w.survey1_token == ls.token)). \
                    filter(and_(ls.startdate != None, ls.submitdate == None)).subquery()
        nowish = nowish_tz()
        results = self.wppdev_db.session.query(u2.c.wikipedia_id, u2.c.survey1_token, u2.c.token, u2.c.startdate, u2.c.submitdate, lt.tid, lt.validfrom, lt.validuntil). \
                    select_from(outerjoin(u2,lt,lt.token == u2.c.survey1_token)).filter(lt.validuntil >= nowish)

        return results
    
    def survey1_expired(self):
        # in wpp_participants, corresponding token in lime_survey_? but lime_survey_?.submitdate is null, and lime_tokens.validuntil has passed
        
        """
            SELECT u2.wikipedia_id, u2.survey1_token, u2.token, u2.startdate, u2.submitdate, lt.tid, lt.validfrom, lt.validuntil FROM 
            (SELECT w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate FROM wpp_participants w LEFT JOIN lime_survey_88653 ls ON w.survey1_token = ls.token WHERE ls.startdate is NOT NULL AND ls.submitdate is NULL) as u2 
            LEFT JOIN lime_tokens_88653 lt ON u2.survey1_token = lt.token
            WHERE lt.validuntil < UTC_TIMESTAMP();  
        """
        w = aliased(WppParticipant)
        ls = aliased(LimeSurvey)
        lt = aliased(LimeTokens)
        #results = self.wppdev_db.session.query(w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate).join((ls, w.survey1_token == ls.token)). \
        #            filter(ls.submitdate == None).all()
        u2 = self.wppdev_db.session.query(w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate).select_from(outerjoin(w,ls, w.survey1_token == ls.token)). \
                    filter(and_(ls.startdate != None, ls.submitdate == None)).subquery()
        nowish = nowish_tz()
        results = self.wppdev_db.session.query(u2.c.wikipedia_id, u2.c.survey1_token, u2.c.token, u2.c.startdate, u2.c.submitdate, lt.tid, lt.validfrom, lt.validuntil). \
                    select_from(outerjoin(u2,lt,lt.token == u2.c.survey1_token)).filter(lt.validuntil < nowish)

        return results
        
    def survey1_done(self):
        # in wpp_participants, corresponding token in lime_survey_? where lime_survey_?.submitdate is not null.
        """
            SELECT u2.wikipedia_id, u2.survey1_token, u2.token, u2.startdate, u2.submitdate, lt.tid, lt.validfrom, lt.validuntil FROM 
            (SELECT w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate FROM wpp_participants w LEFT JOIN lime_survey_88653 ls ON w.survey1_token = ls.token) as u2 
            LEFT JOIN lime_tokens_88653 lt ON u2.survey1_token = lt.token
            WHERE u2.submitdate is not NULL;
        """
        w = aliased(WppParticipant)
        ls = aliased(LimeSurvey)
        lt = aliased(LimeTokens)

        #u2 = self.wppdev_db.session.query(w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate).outerjoin((ls, w.survey1_token == ls.token)). \
        #            filter(ls.submitdate == None).subquery()
        u2 = self.wppdev_db.session.query(w.wikipedia_id, w.survey1_token, ls.token, ls.startdate, ls.submitdate).outerjoin((ls, w.survey1_token == ls.token)).subquery()        
        # results to be just read from u2
        # results = self.wppdev_db.session.query(u2.c.wikipedia_id, u2.c.survey1_token)
        # join lt with u2
        nowish = nowish_tz()
        results = self.wppdev_db.session.query(lt.tid, lt.validfrom, lt.validuntil,
                                               u2.c.wikipedia_id, u2.c.survey1_token, u2.c.token, u2.c.startdate, u2.c.submitdate). \
                    outerjoin((u2,lt.token == u2.c.survey1_token)).filter(u2.c.submitdate != None)

        return results

    def state_by_strata(self, fill_out=True, include_special=True):
        """
        return a dict keyed by stratum, which in turn, is sub-keyed by invitation state, to hold all the accounts for
        each stratum/invitation state (e.g., stratum 0/SURVEY1_STARTED)
        
        fill_out = fill out dict with all the strata and invitation states even if there are no invitations in those slots
        include_special = whether to include stratum -1 for invitations that we've put in by hand into the db 
        """
        results = dict()
        expected_strata = range(0,len(SAMPLE_STRATA))
        if include_special:
            expected_strata += (-1,) # using -1 to mark the special stratum
        expected_states = PARTICIPANT_STATES
        
        # create a dict with every expected stratum and every expected state within a stratum
        # (list() for x in count()) is a generator that yields a new empty list iteration
        if fill_out:
            results = dict([(stratum,
                             dict(izip(expected_states,
                                       (list() for x in count())))) 
                             for stratum in expected_strata])
        else:
            defaultdict_of_lists = partial(defaultdict, list) #create a callable that will return defaultdict(list)
            results = defaultdict(defaultdict_of_lists)
        
        for p in self.invitees():
            results[p.stratum][self.state_of_invitation(p.wikipedia_id)].append(p.wikipedia_id)
          
        return results  
    def counts_state_by_strata(self, fill_out=True, include_special=True):
        """
        tally the number of invitations in each stratum/state 
        """
        m = self.state_by_strata(fill_out=fill_out, include_special=include_special)
        # replace list of Wikipedia ids with the length of the list
        return dict([(strat,
                       dict([(state, len(l)) for (state,l) in v.iteritems()]))
                      for (strat,v) in m.iteritems()])

    def lime_survey_items(self):
        items = self.wppdev_db.session.query(LimeSurvey).all()
        return [(item.id, item.startdate, item.token) for item in items]
    def lime_tokens_items(self):
        # a lot of the fields look not to be used.
        # how is a row in lime_tokens_? generated again?
        # right now, I insert a row with token, validfrom, validuntil
        items = self.wppdev_db.session.query(LimeTokens).all()
        return [(item.tid, item.token, item.validfrom, item.validuntil) for item in items]
    def special_samples(self):
        ids = self.wppdev_db.session.query(WppSpecialSampling).all()
        for id in ids:
            yield id

def print_user_status():
    dashboard = Dashboard()
    print "number of invitations issued in total: ", dashboard.number_of_invitations()

    for p in dashboard.invitees():
        print p.wikipedia_id, p.survey1_token, p.recruit_msg_timestamp
        
    # get invited
    print "\ninvited"
    results = dashboard.invited()
    for result in results:
        print result.wikipedia_id, result.survey1_token, result.validfrom, result.validuntil
        #print type(result), repr(result)
        #print "result.keys()", result.keys()

    print "\nsurvey1_started"
    results = dashboard.survey1_started()
    for result in results:
        print result.wikipedia_id, result.survey1_token, result.validfrom, result.validuntil
        #print "result.keys()", result.keys()
       
    # expired_invitation
    print "\nexpired_invitation"
    results = dashboard.expired_invitation()
    for result in results:
        print result.wikipedia_id, result.survey1_token, result.validfrom, result.validuntil

    # survey1_done
    print "\nsurvey1_done"
    results = dashboard.survey1_done()
    for result in results:
        print result.wikipedia_id, result.survey1_token, result.validfrom, result.validuntil
 
    # survey1_done
    print "\nsurvey1_expired"
    results = dashboard.survey1_expired()
    for result in results:
        print result.wikipedia_id, result.survey1_token, result.validfrom, result.validuntil
       
    # calculate the state of 'RaymondYee'
    print dashboard.state_of_invitation('RaymondYee')
    print dashboard.state_of_invitation('Junk')
    print dashboard.state_of_invitation('NextToGodliness')

def send_email_to_wikipedia_accounts(recipients, mw, subject, body, cc=False):
    """Send email message to recipients"""
    
    results = []
    
    for user in recipients:
        try:
            result = mw.email2(user=user, text=body, subject=subject, cc = cc)
            results.append(result)
        except Exception, e:
            results.append(e)
    
    return results
 
    
class SurveyInviter(object):
    def __init__(self, mwclient, subject, body, cc=False):
        """
        mwclient = instance of mwclient for sending the Wikipedia email
        subject = subject of email
        body = body of email
        cc = whether to cc the sender
        
        """
        self.wppdev_db = wppdev()
        self._SURVEY_URL = SURVEY_URL
        self.mw = mwclient
        self.subject = subject
        self.body = body
        self.cc = cc
    def invite(self,user_names, send_invite=True):
        """
        user_names: list of wikipedia accout names to create  send invitations to
        send_invite: whether to actually send email out with the invitation, in addition to creating database token (default True) 
        """
        
        status = []
        
        for (user_name, user_stratum, user_count_contrib) in user_names:
            # generate a token (worry about token collision?)
            # token of length 9 to 12 using lowercase letters and digits.
            token_len = random.randrange(9,13)
            token = ''.join(random.choice(string.ascii_lowercase + string.digits) for x in range(token_len))
            survey_link = self._SURVEY_URL % (SURVEY_ID, token)
            # try to write the invitation and token into the db
            
            tz_PT = pytz.timezone("US/Pacific")
            # nowish = datetime.datetime(*datetime.datetime.utcnow().timetuple()[:6])
            # place in the timezone of the server (Pacific time)
            nowish = nowish_tz()
            
            participant = WppParticipant()
            participant.wikipedia_id = user_name
            participant.survey1_token = token
            participant.recruit_msg_timestamp = nowish
            participant.stratum = user_stratum
            participant.count_contrib = user_count_contrib
            # print "new participant: %s, %s, %s" %(participant.wikipedia_id, participant.survey1_token, participant.recruit_msg_timestamp)
            
            ltoken = LimeTokens()
            ltoken.token = token
            ltoken.language = "en"
            ltoken.validfrom = nowish
            # add 14 days
            ltoken.validuntil = nowish + datetime.timedelta(14)
            # print "new ltoken: %s, %s, %s, %s" %(ltoken.token, ltoken.language, ltoken.validfrom, ltoken.validuntil)
            
            token_db_success = False
            error_message = ""
            
            # write the tokens transactionally
            try:
                self.wppdev_db.session.add(participant)
                self.wppdev_db.session.add(ltoken)
                self.wppdev_db.session.commit()
                # print "success: added participant, ltoken"
                token_db_success = True
            except Exception, e:
                # print "problem in adding participant, token: ", e
                error_message += "problem in adding participant, token: " + str(e) + "\n"
                try:
                    self.wppdev_db.session.rollback()
                    # print "rollback success"
                    error_message += " rollback success"
                except Exception, e2:
                    error_message += " problem in rolling back: " + str(e2)
                    # print "problem in rolling back: ", e2
            
            # if there is success, send email
            mail_result = None
            
            if token_db_success and send_invite:
                mail_result = self.mw.email2(user=user_name, text=self.body % {'survey_link':survey_link}, subject=self.subject, cc = self.cc)
                error_message += str(mail_result)
                        
            status.append({'user_name':user_name, 'success':token_db_success, 'error_message':error_message, 'mail_result':mail_result})
            
        return status

class SureveyInviterTest(unittest.TestCase):
    def test_make_token_without_email(self):
        mw = mwclient.Site("en.wikipedia.org")
        mw.login(WIKIPEDIA_USER, WIKIPEDIA_PW)        
        inviter = SurveyInviter(mw,INVITATION_SUBJECT_LINE, INVITATION_BODY, cc=True)
        invitees = [('RaymondYee',-1,None)]
        k = inviter.invite(invitees, send_invite=False)
        #check for token in db
        #don't know how to test for the non-sending of email
    def test_send_email_to_wikipedia_accounts(self):
        mw = mwclient.Site("en.wikipedia.org")
        mw.login(WIKIPEDIA_USER, WIKIPEDIA_PW)
        recipients = ['RaymondYee']
        results = send_email_to_wikipedia_accounts(recipients, mw, "test email", "test email body", cc=False)
        print results


def suite():
    
    testcases = []
    suites = unittest.TestSuite([unittest.TestLoader().loadTestsFromTestCase(testcase) for testcase in testcases])
    suites.addTest(SureveyInviterTest('test_send_email_to_wikipedia_accounts')) 
    return suites        
    
def main():
    mw = mwclient.Site("en.wikipedia.org")
    mw.login(WIKIPEDIA_USER, WIKIPEDIA_PW)
    
    inviter = SurveyInviter(mw,INVITATION_SUBJECT_LINE, INVITATION_BODY, cc=True)
    invitees = []
    invitees = [('RaymondYee',-1,None),('NextToGodliness',-1,None)]
    #invitees = ['odednov', 'Jantin', 'Cheshirecat1337']
    #invitees = ['Jantin', 'Cheshirecat1337']
    #invitees = ['Cheshirecat1337']

    inviter.invite(invitees,send_invite=True)
    print_user_status()    
    

if __name__ == '__main__':
    #main()
    suites = suite()
    suites = unittest.defaultTestLoader.loadTestsFromModule(__import__('__main__'))
    unittest.TextTestRunner().run(suites)

    
    
    
    
    

        
        
    

        
        

# -*- coding: UTF-8 -*-

"""
a script to do the stratified sampling of new Wikipedia user accounts.

"""

from wpp_settings import WPPDEV_DB, WPPDEV_PW, WPPDEV_REMOTE_USER, WPPDEV_HOST, WPPDEV_PORT, SAMPLE_STRATA

import contextlib
import itertools
import logging
LOG_FILENAME = 'stratified_sample.log'
import random

import MySQLdb
import MySQLdb.cursors

from subprocess import Popen, PIPE
import shlex, subprocess
from string import strip
import re
import time

import os
from wpp_newusers import wpp_db2

import unittest

# filemode='w' to recreate file

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename=LOG_FILENAME,
                    filemode='w')
logging.debug('This message should go to the log file')


# Let's subclass wpp_db2 to add sampling functionality
class wpp_db3(wpp_db2):
    def get_eligible_for_sampling_using_view(self,limit=100000):
        """using the view """
        SQL = "SELECT * from eligible_for_sampling;"
        cur = self.conn.cursor()
        cur.execute(SQL)
        for user in itertools.islice(cur,limit):
            yield user
    def prep_sample_views(self, ref_time="utc_timestamp()"):
        """put in place some views needed to be used to compute user samples"""
        """my first pass is to hard code the reference time to be now:  utc_timestamp()....but I want to generalize"""
        cur = self.conn.cursor()

# calculate user accounts that were registered in the 4th weeks ago and tally by number of contributions in the
# first 3 weeks of activity for the account

        CREATE_VIEW1_SQL = """
            CREATE OR REPLACE VIEW `sample_users_non_zero_contribs`
            AS select
               `u`.`name` AS `name`,
               `u`.`registration` AS `registration`,
               `u`.`gender` AS `gender`,
               `u`.`groups` AS `groups`,count(`u`.`name`) AS `count_contrib`
            from (`wpp_usercontribs` `c` 
            left join `wpp_newusers` `u` on((`c`.`user_name` = `u`.`name`)))
            where ((`u`.`missing` <> 1) and (`u`.`emailable` = 1) and 
            (`u`.`registration` between
                     (utc_date() - interval (weekday(utc_date()) + 28) day) and 
                    (utc_date() - interval (weekday(utc_date()) + 21) day)) and 
            isnull(`u`.`blockedby`) and (timestampdiff(SECOND,`u`.`registration`,`c`.`timestamp`) <= 3*7*24*3600)) 
            group by `u`.`name` order by count(`u`.`name`);"""

#

        CREATE_VIEW2_SQL = """
            CREATE OR REPLACE VIEW `all_sample_users_usercontrib_0`
            AS select
               `u`.`name` AS `name`,
               `u`.`registration` AS `registration`,
               `u`.`gender` AS `gender`,
               `u`.`groups` AS `groups`,0 AS `count_contrib`
            from `wpp_newusers` `u`
            where ((`u`.`registration` is not null) and (`u`.`missing` <> 1) and (`u`.`emailable` = 1) and (`u`.`registration` between (utc_date() - interval (weekday(utc_date()) + 28) day) and (utc_date() - interval (weekday(utc_date()) + 21) day)) and isnull(`u`.`blockedby`));        
            """

        CREATE_SAMPLE_USERS_TABLE_SQL = """
            CREATE TABLE IF NOT EXISTS `users_in_sample_period` (
              `sample_id` int(10) NOT NULL AUTO_INCREMENT,
              `name` varbinary(255) NOT NULL,
              `registration` datetime DEFAULT NULL,
              `gender` tinytext,
              `groups` text,
              `count_contrib` mediumint(8) unsigned DEFAULT NULL,
               PRIMARY KEY (`sample_id`),
               UNIQUE KEY `sample_name_index` (`name`)
            ) ENGINE=MyISAM AUTO_INCREMENT=866421 DEFAULT CHARSET=utf8;    
                    
            """

        CLEAR_SAMPLE_USER_SQL = """DELETE FROM users_in_sample_period;"""
        INSERT_SAMPLES_SQL = """
            INSERT INTO users_in_sample_period (name, registration, gender, groups, count_contrib)
            SELECT S.name, S.registration, S.gender, S.groups, S.count_contrib FROM 
                    (SELECT R.*, count(R.name) as count FROM 
                            (SELECT * FROM sample_users_non_zero_contribs 
                                            UNION
                    SELECT * FROM all_sample_users_usercontrib_0) as R GROUP BY R.name) as S WHERE S.count = 1
            
            UNION
            
            SELECT * FROM sample_users_non_zero_contribs;         
            """

        cur.execute(CREATE_VIEW1_SQL)
        cur.execute(CREATE_VIEW2_SQL)
        cur.execute(CREATE_SAMPLE_USERS_TABLE_SQL)
        cur.execute(CLEAR_SAMPLE_USER_SQL)
        cur.execute(INSERT_SAMPLES_SQL)
        
        
    def get_eligible_for_sampling(self,limit=100000,ref_time="utc_timestamp()",recalculate_samples=False):
        """my first pass is to hard code the reference time to be now:  utc_timestamp()....but I want to generalize"""

        if recalculate_samples:
            self.prep_sample_views()
    
        cur = self.conn.cursor()    
        SQL = r"""SELECT S.name, S.registration, S.gender, S.groups, S.count_contrib FROM users_in_sample_period S;"""    
        cur.execute(SQL)
        for user in itertools.islice(cur,limit):
            yield user

    def get_eligible_for_sampling_v0(self,limit=10000000,count_contrib_min=None,count_contrib_max=None,ref_time="utc_timestamp()"):
        """An old version of an algorithm to get Wikipedia accounts eligible for sampling"""
        # TO DO: implement count_contrib_min, count_contrib_max
        # sampling length: 15 days (in seconds)
        sampling_length = 15*24*3600 
        # age range of permissible accounts = 15 days to 30 days
        min_account_age = sampling_length
        max_account_age = 30*24*3600

        if (count_contrib_min is None and count_contrib_max is None):
            SQL = """SELECT u.name, u.registration, u.gender, u.groups, count(u.name) AS count_contrib
                        FROM (wpp_usercontribs c left join wpp_newusers u on(c.user_name = u.name))
                        WHERE (u.missing <> 1 AND u.emailable = 1 AND (timestampdiff(SECOND,u.registration,%s) between %d and %d)
                            AND isnull(u.blockedby)) AND timestampdiff(SECOND,u.registration, c.timestamp) <= %d
                        GROUP BY u.name
                        ORDER BY count_contrib ASC;""" % (ref_time,min_account_age,max_account_age,sampling_length)
        elif (count_contrib_min is not None and count_contrib_max is not None):             
            SQL = """SELECT * FROM (SELECT u.name, u.registration, u.gender, u.groups, count(u.name) AS count_contrib
                        FROM (wpp_usercontribs c left join wpp_newusers u on(c.user_name = u.name))
                        WHERE (u.missing <> 1 AND u.emailable = 1 AND (timestampdiff(SECOND,u.registration,%s) between %d and %d)
                            AND isnull(u.blockedby)) AND timestampdiff(SECOND,u.registration, c.timestamp) <= %d
                        GROUP BY u.name ORDER BY count_contrib ASC) as tbl
                        WHERE tbl.count_contrib BETWEEN %d AND %d;""" % (ref_time,min_account_age,max_account_age,sampling_length, count_contrib_min, count_contrib_max)
        # count_contrib_min exists but not count_contrib_max
        elif (count_contrib_min is not None and count_contrib_max is None):
            SQL = """SELECT * FROM (SELECT u.name, u.registration, u.gender, u.groups, count(u.name) AS count_contrib
                        FROM (wpp_usercontribs c left join wpp_newusers u on(c.user_name = u.name))
                        WHERE (u.missing <> 1 AND u.emailable = 1 AND (timestampdiff(SECOND,u.registration,%s) between %d and %d)
                            AND isnull(u.blockedby)) AND timestampdiff(SECOND,u.registration, c.timestamp) <= %d
                        GROUP BY u.name ORDER BY count_contrib ASC) as tbl
                        WHERE tbl.count_contrib >= %d;""" % (ref_time,min_account_age,max_account_age,sampling_length, count_contrib_min)
        elif (count_contrib_min is None and count_contrib_max is not None):
            SQL = """SELECT * FROM (SELECT u.name, u.registration, u.gender, u.groups, count(u.name) AS count_contrib
                        FROM (wpp_usercontribs c left join wpp_newusers u on(c.user_name = u.name))
                        WHERE (u.missing <> 1 AND u.emailable = 1 AND (timestampdiff(SECOND,u.registration,%s) between %d and %d)
                            AND isnull(u.blockedby)) AND timestampdiff(SECOND,u.registration, c.timestamp) <= %d
                        GROUP BY u.name ORDER BY count_contrib ASC) as tbl
                        WHERE tbl.count_contrib <= %d;""" % (ref_time,min_account_age,max_account_age,sampling_length, count_contrib_max)

        # count_contrib_max exists but not count_contrib_min
        cur = self.conn.cursor()
        cur.execute(SQL)
        for user in itertools.islice(cur,limit):
            yield user


@contextlib.contextmanager
def make_client():
    c=main_wpp_db()
    try: yield c
    finally: c._kill_ssh_tunnel()
    
class main_wpp_db(object):
    def __init__(self, user="root", pw=WPPDEV_PW, db=WPPDEV_DB, host=WPPDEV_HOST, remote_user=WPPDEV_REMOTE_USER, port=WPPDEV_PORT):
        self.user = user
        self.pw = pw
        self.db = db
        self.host = host
        self.port = port
        self.remote_user = remote_user
        self.ssh_command = "ssh -fNg -L %d:127.0.0.1:3306 %s" % (self.port, self.remote_user)
        self._establish_ssh_tunnel()
        self.conn = self._make_conn()
    def _make_conn(self):
        return MySQLdb.connect(host=self.host, port=self.port, user=self.user, passwd=self.pw, db=self.db, cursorclass=MySQLdb.cursors.DictCursor, use_unicode=True,
                           charset = "utf8")
    def _establish_ssh_tunnel(self):
        args = shlex.split(self.ssh_command)
        process = subprocess.Popen(args, shell=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        time.sleep(2)
        # pause a bit to make sure it takes hold
        
    def _kill_ssh_tunnel(self):
        time.sleep(2) # pause
        pid = self._pid_of_tunnel()
        kill_command = "kill -9 %s" % (pid)
        process = subprocess.Popen(kill_command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    def _pid_of_tunnel(self):
        p1 = Popen('ps -aef', stdout=PIPE, shell=True)
        p2 = Popen('grep "ssh -fNg -L %d"' % (self.port), stdin=p1.stdout, stdout=PIPE, shell=True)
        p1.stdout.close()  # Allow p1 to receive a SIGPIPE if p2 exits.
        output = p2.communicate()[0]
        pid = None
      
        # grab the appropriate return line
        lines = map(strip, output.split("\n"))
        for line in lines:
            if line.find(self.ssh_command) > 0:
                pid = re.split("\s+",line)[1] # second field
        return pid
    def participants(self):
        SQL = "SELECT * from wpp_participants;"
        cur = self.conn.cursor()
        cur.execute(SQL)
        for participant in cur:
            yield participant
        
       
def partition_into_strata(divs, recalculate_samples=False):
    """
    divs = a list representing the strata -- e.g., [(0,1), (2,4), (5,8), (9,14), (15,100000000000000000000)] to represent
       0-1, 2-4, 5-8, 9-14, 15+
    
    """
    # where do these partitions come from? -- see https://docs.google.com/document/d/1FNCrsCEL0qx9Z9s3IdURL6dNs_KA6FUQm9fLlTqkUCI/edit?hl=en_US
    # It'd be useful to calculate the %ages based on real data.

    # strata is a list of list of accounts, partitioned by strata
    strata = [[] for a in range(len(divs))]  # initialize empty strata based on divs 
 
    db = wpp_db3()
    
    # loop through the 
    current_stratum = 0
    for (i,user) in enumerate(db.get_eligible_for_sampling(limit=None, recalculate_samples=recalculate_samples)):
        # walk through the contributions and group into tiers
        # print i, user["name"], user["registration"], user["gender"], user["groups"], user["count_contrib"]
        while not ((user["count_contrib"] >= divs[current_stratum][0]) and (user["count_contrib"] <= divs[current_stratum][1])):
            current_stratum += 1
        strata[current_stratum].append(user)
    
    return strata

def sample(divs, recalculate_samples=False):
    """
    prints out a random sample of accounts to sample -- right now, sample_n from each stratum
    
    Parameters:
    divs: a list representing the strata -- e.g., [(0,1), (2,4), (5,8), (9,14), (15,100000000000000000000)] to represent
       0-1, 2-4, 5-8, 9-14, 15+
    recalculate_samples: whether to recalculate the samples or use the ones that are cached in the db (default: False)
    """

    strata = partition_into_strata(divs, recalculate_samples=recalculate_samples)
    # check borders of strata
    print [len(k) for k in strata]
    
    # have a random number (what's a good algorithm for selecting m random from total of n items?)
    
    sample_n = 4
    for (k,stratum) in enumerate(strata):
        print k, random.sample(stratum,sample_n)

def test_tunnel_access():
    """
    This function was used when RY ran this code from his laptop but the db accessed was on granite.
    """
    with make_client() as main_db:
        print list(main_db.participants())
        #print main_db._pid_of_tunnel()


if __name__ == '__main__':
    sample(SAMPLE_STRATA, recalculate_samples=True)


    
    
            
    
    
    

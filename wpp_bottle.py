from bottle import route, run, response, request, template, static_file
import os

import mwclient
from wpp_settings import WPPDEV_DB, WPPDEV_PW, WPPDEV_REMOTE_USER, WPPDEV_HOST, WPPDEV_PORT, SAMPLE_STRATA
from wpp_settings import WPPRY_USER, WPPRY_PW, WPPRY_DB, WPPRY_HOST, WPPRY_PORT
from wpp_settings import WIKIPEDIA_USER, WIKIPEDIA_PW, INVITATION_SUBJECT_LINE, INVITATION_BODY

from wpp_dashboard import Dashboard, SurveyInviter
from stratified_sample import partition_into_strata

THIS_DIR = os.path.dirname(os.path.realpath(__file__))

@route('/images/<filename:re:.*\.png>#')
def send_image(filename):
    return static_file(filename, root="%s/images" % (THIS_DIR), mimetype='image/png')

@route('/static/<filename:path>')
def send_static(filename):
    return static_file(filename, root="/%s/static" % (THIS_DIR))

@route('/')
def index():
    return 'hi there. directory of this file is %s' % (THIS_DIR)

@route('/sample')
def sample():
    """
    Calculate and display accounts that we could possibly send invitations to, divided among the sampling strata
    """
    
    response.set_header('Content-Type', 'text/html')

    # first pass: let's run the sampler and display the strata
    divs = SAMPLE_STRATA
    
    # 
    strata = partition_into_strata(divs, recalculate_samples=False)
    
    # pull up list of accounts that we have in the special sampling list
    dashboard = Dashboard()
    
    # obtain the "special" accounts, ones that we might want to sample but are not from the typical sampling algorithm
    special_sample_ids = dashboard.special_samples()
    
    return template("sample", strata=strata, divs=divs, special_sample_ids=special_sample_ids)
    
@route('/sample', method='POST')
def sample_post():
    to_invite = request.forms.getall('to_invite')
    
    mw = mwclient.Site("en.wikipedia.org")
    mw.login(WIKIPEDIA_USER, WIKIPEDIA_PW)
    inviter = SurveyInviter(mw,INVITATION_SUBJECT_LINE, INVITATION_BODY, cc=True)
    
    results = inviter.invite(to_invite)
    return template("sample_post", results=results)
    

@route('/dashboard')
def dashboard():
    response.set_header('Content-Type', 'text/html')
    
    dashboard = Dashboard()
    return template("dashboard", dashboard=dashboard)

if __name__ == '__main__':
    run(host='granite.ischool.berkeley.edu', port=8080, reloader=True)

# need a login interface to enforce access



